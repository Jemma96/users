# Running the Backend (JSON Server)

### `yarn run serve-json`

Runing on [http://localhost:5000].

# Running the Frontend

### `yarn dev`

Run the app in the development mode.\
Open [http://localhost:3000] to view it in the browser.
