import axios from "axios";
import config from "../config/config";

const baseURL = config.VITE_APP_BASE_URL;

const instance = axios.create({ baseURL });

export default instance;
