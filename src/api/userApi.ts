import { IUser } from "../types/global.types";
import axios from "./axios";

export const getUsers = async () => {
  const response = await axios.get("");
  return response.data;
};

export const deleteUserById = async (userId: string) => {
  try {
    await axios.delete(userId);
  } catch (error) {
    console.error("Error updateing user:", error);
  }
};

export const updateUser = async (user: IUser) => {
  try {
    await axios.put(user.id, user);
  } catch (error) {
    console.error("Error updateing user:", error);
  }
};

export const addUser = async (user: IUser) => {
  try {
    await axios.post<IUser>("", user);
  } catch (error) {
    console.error("Error adding user:", error);
  }
};
