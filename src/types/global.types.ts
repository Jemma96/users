export enum Role {
  ADMIN = "Admin",
  USER = "User",
  ALL = "All",
}

export interface IUser {
  id: string;
  firstName: string;
  lastName: string;
  role: Role;
  email: string;
  createdDate: string;
  status: boolean;
}

export type UserCreateProps = Omit<IUser, "id" | "createdDate" | "status">;
