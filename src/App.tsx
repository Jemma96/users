import { QueryClient, QueryClientProvider } from "react-query";
import { RouterProvider } from "react-router";
import routes from "./config/routes";

const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <RouterProvider router={routes} />
    </QueryClientProvider>
  );
}

export default App;
