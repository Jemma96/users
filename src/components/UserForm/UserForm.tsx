import { FC, useEffect } from "react";
import { Col, Form, FormInstance, Input, Row, Select } from "antd";
import { IUser, Role } from "../../types/global.types";
import { validateEmail } from "./utils";
import "./styles.css";

const { Option } = Select;

interface UserFormProps {
  form: FormInstance<IUser>;
  initialData?: IUser;
}

const UserForm: FC<UserFormProps> = ({ form, initialData }) => {
  console.log(initialData, "initialData");

  useEffect(() => {
    if (!initialData) {
      form.resetFields();
    } else {
      form.setFieldsValue({
        firstName: initialData.firstName,
        lastName: initialData.lastName,
        role: initialData.role,
        email: initialData.email,
      });
    }
  }, [initialData]);

  return (
    <Form
      form={form}
      layout="vertical"
      name="userForm"
      initialValues={initialData || {}}
    >
      <Row justify="space-between">
        <Col span={11}>
          <Form.Item
            name="firstName"
            label="First Name"
            rules={[{ required: true, message: "Please enter first name" }]}
            colon={false}
          >
            <Input />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            name="lastName"
            label="Last Name"
            rules={[{ required: true, message: "Please enter last name" }]}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Form.Item
        name="role"
        label="Role"
        rules={[{ required: true, message: "Please select a role" }]}
      >
        <Select>
          <Option value={Role.ADMIN}>Admin</Option>
          <Option value={Role.USER}>User</Option>
        </Select>
      </Form.Item>
      <Form.Item
        name="email"
        label="Email"
        rules={[
          { required: true, message: "Please enter an email" },
          { validator: (_, value) => validateEmail(value) },
        ]}
      >
        <Input />
      </Form.Item>
    </Form>
  );
};

export default UserForm;
