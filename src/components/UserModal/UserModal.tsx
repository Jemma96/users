import { FC } from "react";
import { Modal, Form } from "antd";
import { IUser } from "../../types";
import UserForm from "../UserForm/UserForm";

interface Props {
  open: boolean;
  user?: IUser;
  onCreateUpdate: (values: IUser) => void;
  onCancel: () => void;
}

const UserModal: FC<Props> = ({ open, user, onCreateUpdate, onCancel }) => {
  const [form] = Form.useForm();

  const handleOk = () => {
    form
      .validateFields()
      .then((values) => {
        form.resetFields();
        onCreateUpdate(values);
      })
      .catch((info) => {
        console.log("Validate Failed:", info);
      });
  };

  const handleCancel = () => {
    form.resetFields();
    onCancel();
  };

  return (
    <Modal
      open={open}
      title={!user ? "Create New User" : "Edit User"}
      okText={!user ? "Create" : "Update"}
      cancelText="Cancel"
      onOk={handleOk}
      onCancel={handleCancel}
    >
      <UserForm form={form} initialData={user} />
    </Modal>
  );
};

export default UserModal;
