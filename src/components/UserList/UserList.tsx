import { FC, useEffect, useState } from "react";
import {
  Button,
  Input,
  Row,
  Select,
  Space,
  Switch,
  Table,
  DatePicker,
  Col,
} from "antd";
import {
  EditOutlined,
  DeleteOutlined,
  CloseOutlined,
  ArrowDownOutlined,
  ArrowUpOutlined,
} from "@ant-design/icons";
import { ColumnsType } from "antd/es/table";
import { Filters, Status } from "./types";
import { useMutation, useQuery } from "react-query";
import cn from "classnames";
import { IUser, UserCreateProps, Role } from "../../types/global.types";
import {
  getUsers,
  updateUser,
  deleteUserById,
  addUser,
} from "../../api/userApi";
import dayjs from "dayjs";
import UserModal from "../UserModal/UserModal";
import ConfirmationModal from "../ConfirmationModal/ConfirmationModal";
import "./styles.css";
import { SortOrder } from "antd/es/table/interface";

const { Option } = Select;

const UserList: FC = () => {
  const [dataSource, setDataSource] = useState<IUser[] | undefined>([]);
  const [searchText, setSearchText] = useState<string>("");
  const [roleFilter, setRoleFilter] = useState<Role>(Role.ALL);
  const [statusFilter, setStatusFilter] = useState<Status>(Status.ALL);
  const [modalVisible, setModalVisible] = useState(false);
  const [sortOrder, setSortOrder] = useState<SortOrder>("ascend");
  const [editUser, setEditUser] = useState<IUser | undefined>(undefined);
  const [createdDateFilter, setCreatedDateFilter] = useState<string | null>(
    null
  );
  const [showFilters, setShowFilters] = useState(false);
  const [deleteUserId, setDeleteUserId] = useState<string | undefined>(
    undefined
  );
  const [confirmDeleteVisible, setConfirmDeleteVisible] = useState(false);

  const { data: allUsers, refetch } = useQuery<IUser[]>("users", getUsers);

  const updateMutation = useMutation(updateUser, {
    onSuccess: () => {
      handleReset();
      refetch();
    },
  });

  const addMutation = useMutation(addUser, {
    onSuccess: () => {
      refetch();
    },
  });

  const deleteMutation = useMutation(deleteUserById, {
    onSuccess: () => {
      refetch();
    },
  });

  const handleCancel = () => {
    setEditUser(undefined);
    setModalVisible(false);
  };

  const handleCreateUpdateUser = (values: UserCreateProps) => {
    const data = dataSource as IUser[];
    const newUser: IUser = {
      ...values,
      id: editUser?.id || (+data[data.length - 1].id + 1).toString(),
      status: editUser ? editUser?.status : false,
      createdDate:
        editUser?.createdDate || dayjs().format("YYYY-MM-DD").toString(),
    };

    if (editUser) {
      updateMutation.mutate(newUser);
    } else {
      addMutation.mutate(newUser);
    }
    setModalVisible(false);
  };

  const applyFilters = (filter?: Filters) => {
    let filteredData = allUsers as IUser[];

    if (searchText) {
      filteredData = filteredData.filter(
        (user) =>
          `${user.firstName} ${user.lastName}`
            .toLowerCase()
            .includes(searchText.toLowerCase()) ||
          user.email.toLowerCase().includes(searchText.toLowerCase())
      );
    }

    if (roleFilter !== Role.ALL) {
      if (filter === "role") {
        setRoleFilter(Role.ALL);
      } else {
        filteredData = filteredData?.filter((user) => user.role === roleFilter);
      }
    }

    if (statusFilter !== Status.ALL) {
      if (filter === "status") {
        setStatusFilter(Status.ALL);
      } else {
        filteredData = filteredData.filter((user) =>
          statusFilter === Status.ACTIVE ? user.status : !user.status
        );
      }
    }

    if (createdDateFilter !== null) {
      if (filter === "createDate") {
        setCreatedDateFilter(null);
      } else {
        filteredData = filteredData.filter(
          (user) =>
            user.createdDate === dayjs(createdDateFilter).format("YYYY-MM-DD")
        );
      }
    }

    setDataSource(filteredData);
  };

  const handleDelete = (userId: string) => {
    setDeleteUserId(userId);
    setConfirmDeleteVisible(true);
  };

  const handleConfirmDelete = () => {
    if (deleteUserId) {
      deleteMutation.mutate(deleteUserId);
      setDeleteUserId(undefined);
      setConfirmDeleteVisible(false);
    }
  };

  const handleEdit = (user: IUser) => {
    setEditUser(user);
    setModalVisible(true);
  };

  const handleReset = () => {
    setSearchText("");
    setRoleFilter(Role.ALL);
    setStatusFilter(Status.ALL);
    setCreatedDateFilter(null);
    setDataSource(allUsers);
    setShowFilters(false);
    setEditUser(undefined);
  };

  const handleApply = () => {
    applyFilters();
    setShowFilters(true);
  };

  const handleChangeStatus = (user: IUser) => {
    updateMutation.mutate({ ...user, status: !user.status } as IUser);
  };

  const handleCellClick = () => {
    switch (sortOrder) {
      case "ascend":
        setSortOrder("descend");
        break;
      case "descend":
        setSortOrder(null);
        break;
      default:
        setSortOrder("ascend");
        break;
    }
  };

  useEffect(() => {
    applyFilters();
  }, [searchText]);

  useEffect(() => {
    if (
      roleFilter === Role.ALL &&
      statusFilter === Status.ALL &&
      createdDateFilter === null
    ) {
      setShowFilters(false);
    }

    if (showFilters) {
      applyFilters();
    }
  }, [roleFilter, statusFilter, createdDateFilter, showFilters]);

  useEffect(() => {
    setDataSource(allUsers);
  }, [allUsers]);

  const columns: ColumnsType<IUser> = [
    {
      title: (
        <Row align="middle" className="wt-40">
          <div>Id</div>
          <div>
            {sortOrder === "ascend" && (
              <ArrowDownOutlined className="mr-l-10" />
            )}
            {sortOrder === "descend" && <ArrowUpOutlined className="mr-l-10" />}
          </div>
        </Row>
      ),
      dataIndex: "id",
      key: "id",
      sorter: (a, b) => +a.id - +b.id,
      sortDirections: ["ascend", "descend"],
      defaultSortOrder: "ascend",
      onHeaderCell: () => ({
        onClick: () => {
          handleCellClick();
        },
      }),
    },
    {
      title: "First Name",
      dataIndex: "firstName",
      key: "firstName",
    },
    {
      title: "Last Name",
      dataIndex: "lastName",
      key: "lastName",
    },
    {
      title: "Role",
      dataIndex: "role",
      key: "role",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      render: (email: string) => <a href={`mailto:${email}`}>{email}</a>,
    },
    {
      title: "Created Date",
      dataIndex: "createdDate",
      key: "createdDate",
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      render: (_, record: IUser) => (
        <Switch
          checked={record.status}
          onChange={() => handleChangeStatus(record)}
        />
      ),
    },
    {
      title: "Edit",
      key: "edit",
      render: (_, record: IUser) => (
        <Space size="middle">
          <EditOutlined
            className="pointer"
            onClick={() => handleEdit(record)}
          />
        </Space>
      ),
    },
    {
      title: "Delete",
      key: "delete",
      render: (_, record: IUser) => (
        <Space size="middle">
          <DeleteOutlined
            className="pointer"
            onClick={() => handleDelete(record.id)}
          />
        </Space>
      ),
    },
  ];

  return (
    <div className="users-list">
      <Row className="filters-block">
        <Row justify="space-between" align="middle" className="mr-bm-20">
          <h1>Master Users</h1>
          <div>
            <Button type="primary" onClick={() => setModalVisible(true)}>
              + Create New User
            </Button>
          </div>
        </Row>
        <Row className="mr-bm-20">
          <Input.Search
            placeholder="Search users"
            onChange={(e) => setSearchText(e.target.value)}
            className="wt-200"
            value={searchText}
          />
        </Row>
        <Row className="mr-bm-20" align="bottom">
          <Col>
            <div className="label-text">Status</div>
            <Select
              placeholder="Select status"
              className={cn("mr-r-10", "wt-120")}
              onChange={(value) => setStatusFilter(value)}
              value={statusFilter}
              defaultActiveFirstOption
            >
              <Option value={Status.ALL}>{Status.ALL}</Option>
              <Option value={Status.ACTIVE}>{Status.ACTIVE}</Option>
              <Option value={Status.INACTIVE}>{Status.INACTIVE}</Option>
            </Select>
          </Col>
          <Col>
            <div className="label-text">Role</div>
            <Select
              placeholder="Select role"
              className={cn("mr-r-10", "wt-120")}
              onChange={(value) => setRoleFilter(value as Role)}
              value={roleFilter}
              defaultActiveFirstOption
            >
              <Option value={Role.ALL}>All</Option>
              <Option value={Role.ADMIN}>Admin</Option>
              <Option value={Role.USER}>User</Option>
            </Select>
          </Col>
          <Col>
            <DatePicker
              placeholder="Select created date"
              className={cn("mr-r-10", "wt-200")}
              value={createdDateFilter}
              onChange={(date) => setCreatedDateFilter(date)}
            />
          </Col>
        </Row>
        <Row justify="space-between">
          <div>
            {showFilters && (
              <Row>
                {statusFilter !== Status.ALL && (
                  <Button
                    type="text"
                    onClick={() => applyFilters("status")}
                    className={cn("filter-btn", "mr-r-10")}
                    style={{ display: "flex", alignItems: "center" }}
                  >
                    <span>{statusFilter}</span>
                    <CloseOutlined />
                  </Button>
                )}
                {roleFilter !== Role.ALL && (
                  <Button
                    type="text"
                    onClick={() => applyFilters("role")}
                    className={cn("filter-btn", "mr-r-10")}
                    style={{ display: "flex", alignItems: "center" }}
                  >
                    <span>{roleFilter}</span>
                    <CloseOutlined />
                  </Button>
                )}
                {createdDateFilter !== null && (
                  <Button
                    type="text"
                    onClick={() => applyFilters("createDate")}
                    className={cn("filter-btn", "mr-r-10")}
                    style={{ display: "flex", alignItems: "center" }}
                  >
                    <span>{dayjs(createdDateFilter).format("YYYY-MM-DD")}</span>
                    <CloseOutlined />
                  </Button>
                )}
              </Row>
            )}
          </div>
          <div>
            <Button type="text" onClick={handleReset} className={cn("mr-r-10")}>
              Reset All
            </Button>
            <Button
              type="primary"
              onClick={handleApply}
              className={cn("mr-r-10")}
            >
              Apply
            </Button>
          </div>
        </Row>
      </Row>
      <Table
        columns={columns}
        dataSource={dataSource}
        rowKey={(record) => record.id}
        pagination={{ pageSize: 5 }}
      />
      <UserModal
        open={modalVisible}
        onCreateUpdate={handleCreateUpdateUser}
        onCancel={handleCancel}
        user={editUser}
      />
      <ConfirmationModal
        open={confirmDeleteVisible}
        onConfirm={handleConfirmDelete}
        onCancel={() => setConfirmDeleteVisible(false)}
      />
    </div>
  );
};

export default UserList;
