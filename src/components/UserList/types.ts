export enum Status {
  ACTIVE = "Active",
  INACTIVE = "Inactive",
  ALL = "All",
}

export type Filters = "status" | "role" | "createDate";
