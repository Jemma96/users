import { FC } from "react";
import { Modal } from "antd";

interface ConfirmationModalProps {
  open: boolean;
  onConfirm: () => void;
  onCancel: () => void;
}

const ConfirmationModal: FC<ConfirmationModalProps> = ({
  open,
  onConfirm,
  onCancel,
}) => {
  return (
    <Modal
      open={open}
      title="Confirmation"
      okText="Delete"
      okType="primary"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={onConfirm}
    >
      <p>Are you sure you want to delete this user?</p>
    </Modal>
  );
};

export default ConfirmationModal;
