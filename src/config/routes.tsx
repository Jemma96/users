import { createBrowserRouter } from "react-router-dom";
import ErrorPage from "../pages/ErrorPage/ErrorPage";
import UserPage from "../pages/UserPage/UserPage";

const routes = createBrowserRouter([
  {
    path: "/",
    element: <UserPage />,
  },
  {
    path: "/*",
    element: <ErrorPage />,
  },
]);

export default routes;
