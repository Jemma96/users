const config = ((envs: ImportMetaEnv) => envs)(import.meta.env);

export default config;
