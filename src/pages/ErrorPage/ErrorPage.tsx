import { FC } from "react";
import { useNavigate } from "react-router-dom";
import "./styles.css";
import { Button } from "antd";

const ErrorPage: FC = () => {
  const navigate = useNavigate();

  const handleOnNavigate = () => {
    navigate("/");
  };

  return (
    <div className="error-page">
      <p>404 Page Not Found</p>
      <Button type="primary" onClick={handleOnNavigate}>
        Go Home
      </Button>
    </div>
  );
};

export default ErrorPage;
